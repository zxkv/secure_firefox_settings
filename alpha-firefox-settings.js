/* Set default layout zoom */
pref("layout.css.devPixelsPerPx", "1.0");

/* Disable default browser checking */
pref("browser.shell.checkDefaultBrowser", false);

/* Enable private browsing on startup */
pref("browser.privatebrowsing.autostart", true);

/* Set default homepage to Mozilla */
pref("browser.startup.homepage", "https://www.mozilla.org");

/* Disable warning on close */
pref("browser.tabs.warnOnClose", false);
pref("browser.tabs.warnOnCloseOtherTabs", false);
pref("browser.warnOnQuit", false);

/* Optimize the font style */
pref("font.default.x-western", "sans-serif");
pref("font.minimum-size.x-unicode", "12");
pref("font.minimum-size.x-western", "12");

/* Disable warning from about:config */
pref("general.warnOnAboutConfig", false);

/* Disable blocking onion urls */
pref("network.dns.blockDotOnion", false);

/* Enable Do Not Track Header */
pref("privacy.donottrackheader.enabled", true);

/* Enable Tracking Protection */
pref("privacy.trackingprotection.enabled", true);
pref("privacy.trackingprotection.fingerprinting.enabled", true);
pref("privacy.trackingprotection.cryptomining.enabled", true);

/* Optimize Reader font size */
pref("reader.font_size", "12");

/* Show warning on insecure website */
pref("security.insecure_connection_icon.enabled", true);
pref("security.insecure_connection_text.enabled", true);

/* Disable remember Sign-Ons */
pref("signon.rememberSignons", false);

/* Enable security mechanism */
pref("security.ssl3.dhe_rsa_aes_128_sha", true);
pref("security.ssl3.dhe_rsa_aes_256_sha", true);
pref("security.ssl3.ecdhe_ecdsa_aes_128_gcm_sha256", true);
pref("security.ssl3.ecdhe_ecdsa_aes_128_sha", true);
pref("security.ssl3.ecdhe_ecdsa_aes_256_gcm_sha384", true);
pref("security.ssl3.ecdhe_ecdsa_aes_256_sha", true);
pref("security.ssl3.ecdhe_ecdsa_chacha20_poly1305_sha256", true);
pref("security.ssl3.ecdhe_rsa_aes_128_gcm_sha256", true);
pref("security.ssl3.ecdhe_rsa_aes_128_sha", true);
pref("security.ssl3.ecdhe_rsa_aes_256_gcm_sha384", true);
pref("security.ssl3.ecdhe_rsa_aes_256_sha", true);
pref("security.ssl3.ecdhe_rsa_chacha20_poly1305_sha256", true);
pref("security.ssl3.rsa_aes_128_sha", true);
pref("security.ssl3.rsa_aes_256_sha", true);
pref("security.ssl3.rsa_des_ede3_sha", true);

/* Enable Subresource Integrity security feature */
/* (enables browser to verify that resources they fetch are delivered without unexpected manipulation) */
pref("security.sri.enable", true);

/* Disable Telemetry toolkit */
pref("toolkit.telemetry.enabled", false);

/* Enable syntax highlight on source view */
pref("view_source.syntax_highlight", true);

/* Disable Pocket Extension */
pref("extensions.pocket.enabled", false);

/* Setting zoom max/min percent */
pref("zoom.maxPercent", "300");
pref("zoom.minPercent", "50");
