# Firefox Settings File

Firefox is one of the most popular web browsers. But we all know that many features must be enabled by ourself <br>
to optimize the browsing experience. I have write a firefox settings file which do exactly that. <br>
Changes:

- many security options are now enabled by default
- the browser layout is optimized
- warning on close is now disabled by default
- and more ...

## How to

https://superuser.com/questions/823530/change-some-default-settings-for-firefox-globally

## Final words

Firefox is owned by Mozilla.